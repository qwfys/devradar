<p align="center">
  <img src="assets/logo-text.png">
<p align="center">Track and manage skills as an individual and manage competences across a team.<p>
  <p align="center">
    <a href="./LICENSE">
    	<img src="https://badgen.net/badge/license/MIT/blue" />
    </a>
    <a href="https://gitter.im/devradar/discuss">
	    <img src="https://badgen.net/badge/chat/on%20gitter/cyan?icon=gitter">
    </a>
    <a href="https://vuejs.org/">
	    <img src="https://badgen.net/badge/built%20with/Vue.js/cyan">
    </a>
    <a href="https://www.typescriptlang.org/">
	    <img src="https://badgen.net/badge/code/TypeScript/blue">
    </a>  
    <a href="https://standardjs.com/">
	    <img src="https://badgen.net/badge/code%20style/standard/pink">
    </a>
    <br />
    <a href="https://app.fossa.com/projects/git%2Bgithub.com%2Fanoff%2Fdevradar?ref=badge_shield">
      <img src="https://app.fossa.com/api/projects/git%2Bgithub.com%2Fanoff%2Fdevradar.svg?type=shield" />
    </a>
    <a href="https://dependabot.com/">
      <img src="https://badgen.net/dependabot/dependabot/dependabot-core/?icon=dependabot" />
    </a>
    <a href="https://github.com/anoff/devradar/actions">
	    <img src="https://github.com/anoff/devradar/workflows/editor/badge.svg">
    </a>
    <a href="https://github.com/anoff/devradar/actions">
	    <img src="https://github.com/anoff/devradar/workflows/teams/badge.svg">
    </a>  
    <a href="https://github.com/anoff/devradar/actions">
	    <img src="https://github.com/anoff/devradar/workflows/web/badge.svg">
    </a>
  </p>
</p>


<br />

**Get more information: [devradar homepage](https://devradar.io)** 

**Try it out: [devradar editor](http://editor.devradar.io/)**

This repository holds the individual projects making up the devradar ecosystem:

* [Editor](editor/): track and visualize tech skills (Vue)
* [Teams](teams/): manage skills across an entire team (Vue)
* [Web](web/): devradar landing page at [devradar.io](//devradar.io) (Hugo)

See [anoff/devradar-static](https://github.com/anoff/devradar-static) for an easy to setup version of the devradar to present your own tech skills.

<br />

## Demo

### devradar editor

Create and update your skill information

<img src="assets/editor-demo.gif" alt="video of the editor app">

### devradar teams

Compare your teams target skills with existing team devradars

<img src="assets/teams-demo.gif" alt="video of the teams app">

<br />

## License & Attributions

Copyright 2019 Andreas Offenhaeuser <https://anoff.io>

All devradar code is licensed under [MIT](LICENSE) see [tl;dr; legal](https://tldrlegal.com/license/mit-license) for a quick overview of what this means.

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fanoff%2Fdevradar.svg?type=large)](https://app.fossa.io/projects/git%2Bgithub.com%2Fanoff%2Fdevradar?ref=badge_large)
